package com.baltictalents.tasks;

import com.baltictalents.EnterDataByKeyboard;
import com.baltictalents.FromFile;
import com.baltictalents.HardCodeData;
import com.baltictalents.ReadData;

public class Tasks5 {

    // sprendziant valstybinio egzamino uzduoti, apsibrezti klase kuri sprendzia uzduoti,
    // bet nera priklausoma nuo pradiniu parametru,
    // sprendimas turi veikti skirtingai paduodant parametrus.
   public void task1() {

        ReadData dataEnteredByKeybord = new EnterDataByKeyboard();

        ReadData dataHardcode = new HardCodeData(50, 100, 200);

        ReadData fromFile = new FromFile("U1.txt");


        Tasks4 tasks4 = new Tasks4();

        tasks4.runSolution(fromFile);






    }
}
