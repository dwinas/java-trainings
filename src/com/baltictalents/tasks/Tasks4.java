package com.baltictalents.tasks;

import com.baltictalents.ReadData;
import com.baltictalents.Slidininkas;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class Tasks4 {



    public void runSolution(ReadData initialData){

        List <String> filesInformation = initialData.readedData();

        Map<String, Integer> informationFromFile = listToMap(filesInformation);

        writeResult(informationFromFile);

    }





    private List <String> readFile(){

        File file = new File("C:\\java\\java\\java-trainings\\src\\com\\baltictalents\\resources\\U1.txt");

        List<String> dataFromFile = new ArrayList<>();

        try {
            Scanner scanedFile = new Scanner(file);
            int i = scanedFile.nextInt();

            while (scanedFile.hasNextLine()) {

                String line = scanedFile.nextLine();
                if (line != " "){
                    dataFromFile.add(line);}
            }
            scanedFile.close();
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
        return dataFromFile;
    }

    private Map<String, Integer> listToMap(List<String> list) {

        Map<String, Integer> map = new HashMap<>();

        for(String i:list ) {

            if (i.length()>1) {

                String spalva = i.substring(0, 1);

                i = i.replaceAll("\\D+", "");

                Integer number = Integer.valueOf(i);

                Integer countedAllready = map.get(spalva);

                if (countedAllready != null) {
                    map.put(spalva, countedAllready + number);
                } else {
                    map.put(spalva, number);
                }
            }
        }
        return map;
    }

    private void writeResult(Map<String, Integer> map) {

        Integer zaliuJuosteliuSkaicius = map.get("Z");
        Integer raudonuJuosteliiuSkaicius = map.get("R");
        Integer geltonuJuosteliuSkaicius = map.get("G");
        Integer veleveliuSkaicius = iseisVeleveliu(zaliuJuosteliuSkaicius, raudonuJuosteliiuSkaicius, geltonuJuosteliuSkaicius);

        String likoZaliu = Integer.toString(zaliuJuosteliuSkaicius - veleveliuSkaicius * 2);
        String likoRaudonu = Integer.toString(raudonuJuosteliiuSkaicius - veleveliuSkaicius * 2);
        String likoGeltonu = Integer.toString(geltonuJuosteliuSkaicius - veleveliuSkaicius * 2);
        try {
            PrintWriter atsakymas = new PrintWriter("C:\\java\\java\\java-trainings\\src\\com\\baltictalents\\resources\\U2Rez.txt", "UTF-8");
            atsakymas.println(Integer.toString(veleveliuSkaicius));
            atsakymas.println("G " + likoGeltonu);
            atsakymas.println("Z " + likoZaliu);
            atsakymas.println("R " + likoRaudonu);
            atsakymas.close();

        }
        catch (IOException ex) {
            // Report
        }

    }



    private Integer iseisVeleveliu(Integer r, Integer g, Integer z){

        Integer maziausiasSkaicius = r;
        maziausiasSkaicius = g < r ? g : r;
        maziausiasSkaicius = z < r ? z : r;

        Integer iseisVeleveliu = maziausiasSkaicius % 2 == 0 ? maziausiasSkaicius / 2 : (maziausiasSkaicius - 1) / 2;

        return iseisVeleveliu;
    }


    public void runSlidininkai(String filePath, String pathForAnswer){


        printResultsOfSlidininkai(readFileOfSlidininkas(filePath),pathForAnswer);


    }

//-------------------Slidininku uzduotis--------------------

    private Map<String, Slidininkas> readFileOfSlidininkas(String directory) {

        Map <String, Slidininkas> mapOfSlidininkai = new HashMap<>();


        try{

            File file = new File(directory);
            Scanner scan = new Scanner(file);

            Integer numberOnStart = scan.nextInt();

            for (int i = 0; i < numberOnStart; i++){

                boolean isString = true;
                String name = "";

                // reading name

                while (isString){

                    if (!scan.hasNextInt()){

                        name = name + scan.next() + " ";


                    }
                    else isString = false;
                }

                //reading starting time

                Integer startingHour = scan.nextInt();

                Integer startingMinute = scan.nextInt();


                Integer startingSecond = scan.nextInt();



                Slidininkas slidininkas = new Slidininkas(name);

                slidininkas.setStartingTimeInSeconds(timeToSeconds(startingHour, startingMinute, startingSecond));
                mapOfSlidininkai.put(name, slidininkas);
            }

            //reikia toliau nuskaitineti kurie finisavo i tapati array.

            Integer howManyFinished = scan.nextInt();

            for (Integer j = 0; j < howManyFinished; j++){

                boolean isString = true;
                String name = "";

                while (isString){

                    if (!scan.hasNextInt()){

                        name = name + scan.next() + " ";

                    }
                    else isString = false;
                }

                Integer finishingHour = scan.nextInt();
                Integer finishingMinute = scan.nextInt();
                Integer finishingSecond = scan.nextInt();

                mapOfSlidininkai.get(name).setFinishingTimeInSeconds(timeToSeconds(finishingHour, finishingMinute,finishingSecond));


            }


        }catch (Exception ex){

            System.out.println("Reading file error: " +  ex);
        }

        return  mapOfSlidininkai;

    }

    private void printResultsOfSlidininkai(Map<String, Slidininkas> slidininkai, String directory){


        try{

            PrintWriter writeResult = new PrintWriter(directory, "UTF-8");

            slidininkai
                    .entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.naturalOrder()))
                    .filter(o-> o.getValue().getResult() != 0)
                    .forEach(o -> writeResult.println(o.getValue().toString()));

            writeResult.close();

        }catch (Exception ex){
            System.out.println("Error in writing " + ex);
        }



    }



    Integer timeToSeconds (int hours, int minutes, int seconds){

        Integer timeInSeconds = hours * 3600 + minutes * 60 + seconds;

        return timeInSeconds;
    }


}

