package com.baltictalents.tasks;

import com.baltictalents.Avis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class Tasks6 {

    // VBE 2 uzduotis http://www.nec.lt/failai/5256_IT-VBE-1_2015.pdf
    // rekomenduoju susikurti atskira klase siai uzduociai spresti, naudoti java kolekcijas


    public void task1() {

        List<Avis> listOfAvis = new ArrayList<>();

        listOfAvis = readFile("C:\\java\\java\\java-trainings\\src\\com\\baltictalents\\resources\\avysU2.txt");

        listOfAvis = writeSimilarity(listOfAvis);


        writeFile(listOfAvis, "C:\\java\\java\\java-trainings\\src\\com\\baltictalents\\resources\\avysResult.txt");


    }




   private List <Avis> readFile (String  fileAdress){


       List<Avis> listOfAvis = new ArrayList<>();


       try{
           File file = new File(fileAdress);

           Scanner scaningFile = new Scanner(file);

           Avis.setNumberOfAvis(scaningFile.nextInt());

           Avis.setLenghtOfDnr(scaningFile.nextInt());

           Avis.setCompareToNumber(scaningFile.nextInt());

           // nuskaitom ir sudedam visas avis i aviu lista
           for(int i =0; i < Avis.getNumberOfAvis(); i ++){

               Avis avis = new Avis(scaningFile.next(), scaningFile.next());

               listOfAvis.add(avis);
           }


       } catch (Exception ex){
           System.out.println("Reading " + ex);
       }

return listOfAvis;
    }


   private void writeFile(List<Avis> listOfAvis, String fileName){

            listOfAvis.sort(Comparator.naturalOrder());

        try {

            PrintWriter answer = new PrintWriter(fileName, "UTF-8");

            Avis avis = listOfAvis.get(0);

            answer.println(avis.getName());


            for (int i = 1; i < Avis.getNumberOfAvis(); i++) {

                answer.println(listOfAvis.get(i));
            }


            answer.close();


        }catch (Exception ex){

            System.out.println("Writing " + ex);

        }
    }


    List<Avis> writeSimilarity (List<Avis> avis){

        for(int i = 0; i < Avis.getNumberOfAvis(); i++){

            Integer similarity = 0;

            //Tikrina kiek raidziu atitinka lyginant su etalonines avies DNR
            for (int j = 0;  j < Avis.getLenghtOfDnr(); j++){

                 similarity= avis.get(Avis.getCompareToNumber()-1).getDnr().charAt(j) == avis.get(i).getDnr().charAt(j) ? similarity + 1: similarity;
            }

            avis.get(i).setSimilarity(similarity);

        }
        return avis;
    }


    // VBE 2 uzduotis http://www.nec.lt/failai/1602_IT-pagr-2010.pdf
    // rekomendacijos tos pacios kaip ir task1


    public void task2() {

        Map<String, Integer> patiekalais = new HashMap<>();

        patiekalais = patiekalai("C:\\java\\java\\java-trainings\\src\\com\\baltictalents\\resources\\patiekalaiU3.txt");

        irasytiFaila(patiekalais, "C:\\java\\java\\java-trainings\\src\\com\\baltictalents\\resources\\patiekalaiResult.txt");

    }

   private Map<String, Integer>patiekalai(String filePath){


        Map<String, Integer> patiekalai = new HashMap<>();

        List<Integer> kainos  = new ArrayList<>();


        try{

            File file = new File(filePath);

            Scanner scanner = new Scanner(file);

            Integer produktuSkaicius = scanner.nextInt();

            Integer patiekaluSkaicius = scanner.nextInt();

// Nuskaitom produktu kainas

            for (int i = 0; i < produktuSkaicius; i++){ kainos.add(scanner.nextInt());}


            for (int j = 0; j < patiekaluSkaicius; j++){

//Nuskaitom patiekalo pavadinima
                String patiekaloPavadinimas = scanner.next();

                if (!scanner.hasNextInt()){patiekaloPavadinimas = patiekaloPavadinimas + " " + scanner.next();}

                Integer patiekaloKaina = 0;

                // Suskaiciuojam patiekalo kaina produktu kaina * produkto kiekis patiekale
                for (int k = 0; k < produktuSkaicius; k++){

                    patiekaloKaina += (scanner.nextInt() * kainos.get(k));
                }
//Sudedam viska i map
                patiekalai.put(patiekaloPavadinimas, patiekaloKaina);

            }

            }catch(Exception ex) {
            System.out.println("Klaida nuskaitant " + ex);
        }
                return patiekalai;
            }


            private void irasytiFaila(Map<String, Integer> patiekalai, String failoDirektorija){

                try {

                    PrintWriter answer = new PrintWriter(failoDirektorija, "UTF-8");

                    patiekalai
                            .entrySet().stream()
                            .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                            .forEach(e-> answer.println(e.getKey() + " " + e.getValue()));


                            Integer bendraPatiekaluKaina = patiekalai.values().stream().mapToInt(i->i).sum();

                            Integer bendraPatiekaluKainaEurai = bendraPatiekaluKaina / 100;

                            Integer bendraPatiekaluKainCentai = bendraPatiekaluKaina - bendraPatiekaluKainaEurai * 100;

                            answer.println(bendraPatiekaluKainaEurai + " " + bendraPatiekaluKainCentai);

                            answer.close();

                } catch (Exception ex){

                    System.out.println("Klaida irasant " + ex);
                }

            }

}




