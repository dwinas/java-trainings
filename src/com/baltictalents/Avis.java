package com.baltictalents;

public class Avis implements Comparable<Avis>{

    String name;
    String dnr;
    Integer similarity;

    static Integer numberOfAvis;
    static Integer lenghtOfDnr;
    static Integer compareToNumber;


   public Avis(String name, String dnr){

        this.name = name;
        this.dnr = dnr;
    }


    public String getName() {
        return name;
    }

    public String getDnr() {
        return dnr;
    }

    public Integer getSimilarity() {
        return similarity;
    }

    public static Integer getNumberOfAvis() {
        return numberOfAvis;
    }

    public static Integer getLenghtOfDnr() {
        return lenghtOfDnr;
    }

    public static Integer getCompareToNumber() {
        return compareToNumber;
    }



    public static void setCompareToNumber(Integer compareToNumber) {
        Avis.compareToNumber = compareToNumber;
    }

    public static void setNumberOfAvis(Integer numberOfAvis) {
        Avis.numberOfAvis = numberOfAvis;
    }

    public static void setLenghtOfDnr(Integer lenghtOfDnr) {
        Avis.lenghtOfDnr = lenghtOfDnr;
    }

    public void setSimilarity(Integer similarity) {
        this.similarity = similarity;
    }

    @Override
    public int compareTo(Avis o) {

        if (o.getSimilarity() == similarity){return o.getName().compareTo(name) * -1;}

        else {return o.getSimilarity().compareTo(similarity);}



    }

    @Override
    public String toString() {
        return name + ' ' + similarity;

    }
}


