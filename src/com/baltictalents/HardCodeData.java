package com.baltictalents;

import org.omg.PortableInterceptor.ServerRequestInfo;

import java.util.ArrayList;
import java.util.List;

public class HardCodeData implements ReadData {

    Integer numberOfColorG;
    Integer numberOfColorZ;
    Integer numberOfColorR;


    public HardCodeData(Integer numberOfColorG, Integer numberOfColorZ, Integer numberOfColorR) {
        this.numberOfColorG = numberOfColorG;
        this.numberOfColorZ = numberOfColorZ;
        this.numberOfColorR = numberOfColorR;
    }



    @Override
    public List<String> readedData() {

        List<String>totalNumberOfColors = new ArrayList<>();

        totalNumberOfColors.add("G " + numberOfColorG);
        totalNumberOfColors.add("Z " + numberOfColorZ);
        totalNumberOfColors.add("R " + numberOfColorR);

        return totalNumberOfColors;
    }
}
