package com.baltictalents.lessons;

import com.baltictalents.lessons.work.Traveler;

import java.util.*;

public class Lesson7 {

    // exceptions, try, catch, throw, throws, finally
    // issiaiskinsim kas yra exception kaip dirbti


    void lessonWork() {
        while (true) {
            try {
                exampleUsingTryCatch();
            } catch (ArithmeticException ex) {
                System.out.println(ex.getMessage());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    static void exampleUsingTryCatch() throws Exception {
        Scanner scanner = new Scanner(System.in);
        Integer a = -1;
        a = scanner.nextInt();
        if (a == 0)
            throw new ArithmeticException("Division by zero");

        System.out.println(100 / a);
        System.out.println(a);
    }

    void exampleWithoutTry() {
        Scanner scanner = new Scanner(System.in);
        String a = scanner.next();

        if (a.matches("[0-9]+")) {
            Integer aInteger = new Integer(a);
            if (aInteger == 0)
                System.out.println("/ by zero");
            else
                System.out.println(100 / aInteger);
        } else {
            System.out.println("Not a number");
        }
    }


    // recursion uses stack be carefully
    static void printArray(int[] array, int index) {

        Integer a = 10;

        if (index < array.length) {
            System.out.println(array[index]);
            printArray(array, index + 1);
        }
    }

    void sortObjectUsingComparatorOrCamparable() {
        Traveler t1 = new Traveler("Andzej", 35, 'M');
        Traveler t2 = new Traveler("Eimantas", 83, 'M');
        Traveler t3 = new Traveler("Adomas", 25, 'M');
        Traveler t4 = new Traveler("Ieva", 25, 'F');
        Traveler t5 = new Traveler("Karolina", 23, 'F');
        Traveler t6 = new Traveler("Lavar", 35, 'M');
        Traveler[] travelers = new Traveler[] {t1, t2, t3, t4, t5, t6};

        List<Traveler> travelerList = new ArrayList<>(Arrays.asList(travelers));

        Comparator<Traveler> sortByAge = new Comparator<Traveler>() {
            @Override
            public int compare(Traveler o1, Traveler o2) {
                return o1.getAge() - o2.getAge();
            }
        };

        Comparator<Traveler> sortByName = new Comparator<Traveler>() {
            @Override
            public int compare(Traveler o1, Traveler o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };

        Collections.sort(travelerList);

        for (Traveler i: travelerList) {
            System.out.println(i);
        }
    }
 }
