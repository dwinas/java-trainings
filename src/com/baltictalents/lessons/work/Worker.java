package com.baltictalents.lessons.work;

public class Worker {

    final String name;
    final String surname;
    final String dateOfBirth;
    WorkTools[] listOfOwnedTools;
    Integer yearsOfExperianceInCompany;
    Integer numberOfOwnedTools;

   public Worker(String name, String surname, String dateOfBirth, WorkTools[] listOfOwnedTools, Integer yearsOfExperianceInCompany){

        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.listOfOwnedTools = listOfOwnedTools;
        this.yearsOfExperianceInCompany = yearsOfExperianceInCompany;
        this.numberOfOwnedTools = listOfOwnedTools.length;
    }

    public void printInformation() {

        System.out.println("Name: " + this.name);
        System.out.println("Surname: " + this.surname);
        System.out.println("Date of birth: " + this.dateOfBirth);
        System.out.println("Working in company: " + this.yearsOfExperianceInCompany + "year");

        System.out.println(this.numberOfOwnedTools + " Tools owned:");
        for (WorkTools i : this.listOfOwnedTools) {
            i.printInformation();
        }
    }

}
