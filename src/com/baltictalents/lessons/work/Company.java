package com.baltictalents.lessons.work;

public class Company {

    String name;
    Integer numberOfWorkers;
    Worker director;
    String adress;

   public Company(String name, Integer numberOfWorkers, Worker director, String adress){

        this.name = name;
        this.numberOfWorkers = numberOfWorkers;
        this.director = director;
        this.adress = adress;
    }
    public void printDetails(){
        System.out.println("Company name: " + this.name);
        System.out.println("Company adress: " + this.adress);
        System.out.println("Number of workers: " + this.numberOfWorkers);
        System.out.println("Director " + this.director.name + " " + this.director.surname);

    }

}
