package com.baltictalents.lessons.work;

public class WorkTools {

    String name;
    Double price;
    String purchaseDate;

    public WorkTools(String name, Double price, String purchaseDate){

        this.name = name;
        this.price = price;
        this.purchaseDate = purchaseDate;
    }

    public void printInformation(){
        System.out.println(this.name + " purchase date " + this.purchaseDate + " price " + this.price + " eur.");
    }

}
