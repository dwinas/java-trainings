package com.baltictalents.lessons.polymorphism;

import java.io.File;
import java.util.Map;

public class CurrencyExchanger implements Exchangeable {

    private Map<String, String> currencies;

    public CurrencyExchanger(File currenciesSource) {
        // readFile
    }

    @Override
    public Double convert(String from, String to) {
        return 0.8;
    }
}
