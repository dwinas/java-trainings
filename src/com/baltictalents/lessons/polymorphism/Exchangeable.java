package com.baltictalents.lessons.polymorphism;

public interface Exchangeable {

    Double convert(String from, String to);
}
