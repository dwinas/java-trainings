package com.baltictalents.lessons.polymorphism;

public interface Figure {

    Double area();
    Double perimeter();
}
