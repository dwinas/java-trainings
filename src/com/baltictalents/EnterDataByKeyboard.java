package com.baltictalents;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EnterDataByKeyboard implements ReadData {

    List<String> entryOfcolors(){

        boolean dataIsCorect;
        boolean enterNewColor = true;
        List<String> enteredData = new ArrayList<>();

        while (enterNewColor){

            String newEnteredData = "";

            dataIsCorect =false;

            while (dataIsCorect == false) {

                String color = enterWithKeyboard("Chose color G, Z or R");

                if (color.equals("G") || color.equals("Z") || color.equals("R")){ dataIsCorect = true; newEnteredData = color;}
                else {System.out.println("Entered invalid data!");}
            }


            dataIsCorect =false;

            while (dataIsCorect == false) {

               String numberOfcardsString = enterWithKeyboard("Enter number of cards: ");

               Integer numberOfcards = Integer.parseInt(numberOfcardsString);

                if (numberOfcards >= 0 & numberOfcards instanceof Integer){ dataIsCorect = true; newEnteredData = newEnteredData + " " + numberOfcards;}

                else {System.out.println("Entered invalid data!");}
        }

            enteredData.add(newEnteredData);


            dataIsCorect =false;

            while (dataIsCorect == false) {

             String enterNewData = enterWithKeyboard("Would you like to enter new collor? Y/N");

                if (enterNewData.equals("Y")){dataIsCorect = true; enterNewColor = true;}

                else if (enterNewData.equals("N")){dataIsCorect = true; enterNewColor = false;}

                else System.out.println("Invalid entered data");
            }

        }
        return enteredData;
    }




    String enterWithKeyboard(String request){


        Scanner reader = new Scanner(System.in);

        System.out.println(request);
        String result = reader.nextLine();

    return result;
    }


    @Override
    public List<String> readedData() {
        return entryOfcolors();
    }
}
