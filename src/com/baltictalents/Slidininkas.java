package com.baltictalents;

import java.util.ArrayList;

public class Slidininkas implements Comparable <Slidininkas> {

   final String name;

    Integer startingTimeInSeconds;

    Integer finishingTimeInSeconds;

    Integer result;

    public Slidininkas(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public Integer getStartingTimeInSeconds() {
        return startingTimeInSeconds;
    }

    public Integer getFinishingTimeInSeconds() {
        return finishingTimeInSeconds;
    }

    public void setStartingTimeInSeconds(Integer startingTimeInSeconds) {
        this.startingTimeInSeconds = startingTimeInSeconds;
    }

    public void setFinishingTimeInSeconds(Integer finishingTimeInSeconds) {
        this.finishingTimeInSeconds = finishingTimeInSeconds;
    }


   public Integer getResult(){


        if (finishingTimeInSeconds != null) {
            result = startingTimeInSeconds < finishingTimeInSeconds ? finishingTimeInSeconds - startingTimeInSeconds
                    : finishingTimeInSeconds - startingTimeInSeconds + (24 * 3600);

        }else result = 0;

         return result;
        }


        public String resultToString(){

            String result = "";
            Integer secondsTotal = this.getResult();

            Integer hours = (secondsTotal / 3600);

            Integer minutes = (secondsTotal - 3600 * hours) / 60;

            Integer seconds = (secondsTotal - 3600 * hours - 60 * minutes);

            String secondsInString = seconds > 10 ? Integer.toString(seconds) : "0" + Integer.toString(seconds);



            if (hours > 0 ) result = result + hours + " ";


            result = result + minutes + " " + secondsInString;

            return result;
        }

    @Override
    public int compareTo(Slidininkas o) {

        if (o.getResult().equals(this.getResult())) return o.getName().compareTo(name) * -1;

        else return o.getResult().compareTo(this.getResult()) * -1;

    }

    @Override
    public String toString() {

        return name + " " + this.resultToString();
    }
}










