package com.baltictalents;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class FromFile implements ReadData {

    String fileName;

    public FromFile(String fileName) {
        this.fileName = fileName;
    }

    private List<String> readFile(){

        List<String> dataFromFile = new ArrayList<>();
        try {

            File file = new File("C:\\java\\java\\java-trainings\\src\\com\\baltictalents\\resources\\" + this.fileName);
            Scanner in = new Scanner(file);

            while (in.hasNextLine()) {

                String line = in.nextLine();
                dataFromFile.add(line);
            }
            in.close();
        }
            catch (Exception ex) {
                ex.printStackTrace();
        }
        return dataFromFile;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FromFile)) return false;
        FromFile fromFile = (FromFile) o;
        return Objects.equals(fileName, fromFile.fileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName);
    }

    @Override
    public List<String> readedData() {


        return readFile();
    }

}
