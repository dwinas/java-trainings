public class Uzduotys2 {

    /*
    Duoti trys skaičiai: a, b, c. Nustatykite ar šie skaičiai gali būti
    trikampio kraštinių ilgiai ir jei gali tai kokio trikampio:
    lygiakraščio, lygiašonio ar įvairiakraščio. Atspausdinkite
    atsakymą. Kaip pradinius duomenis panaudokite tokius skaičius:
    3, 4, 5
    2, 10, 8
    5, 6, 5
    5, 5, 5
     */
    void task1() {

        Integer[][] triangles = {
                {
                    3, 4, 5
                },
                {
                    2, 10, 8
                },
                {
                    5, 6, 5
                },
                {5, 5, 5}
        };

        // input these values using scanner;


        for (Integer[] triangleSides: triangles) {
            Integer a = triangleSides[0];
            Integer b = triangleSides[1];
            Integer c = triangleSides[2];
            String result = checkTriangleType(a, b, c);
            System.out.println("a =  "+ a + "; b = " + b + "; c = " + c + "; " + result);
        }
    }

    String checkTriangleType(Integer a, Integer b, Integer c) {

        String result = "";

        if ((a + b) <= c || (b + c) <= a || (c + a) <= b){ result = "Trangle doesn't exist";}

        else if (a == b & b == c){ result = "Trangle is equilateral";}

        else if (a == b || b == c || c == a){ result= "Trangle is isosceles";}

        else if (a * a + c * c == b * b || b * b + a * a == c * c || c * c + b * b == a * a){ result= "Trangle is right";}

        else {result = "Trangle is Scalene";}

        return result;
        }


    /*
    Apskaiciuoti trikampiu plotus
    3, 4, 5
    2, 10, 8
    5, 6, 5
    5, 5, 5
     */
    void task2() {

        Integer [] [] triangle = {
                {3, 4, 5},
                {2, 10, 8},
                {5, 6, 5},
                {5, 5, 5}
        };
        Integer trangleCount = 0;

        for (Integer[] i:triangle){

            trangleCount +=1;
            double perimetre = i[0] + i[1] + i[2];
            double p = perimetre/2;
            double firstPart = p * (p-i[0]) * (p-i[1]) * (p-i[2]);
            if(firstPart > 0 ) {
                System.out.println("Trikampio nr " + trangleCount + " plotas: " + Math.sqrt(firstPart));
            }
            else {
                System.out.println("Trikampio nr " + trangleCount + " parametrai neteisingi" );

            }
         }
    }

    // duotas sveikuju skaiciu masyvas
    // surasti min ir max
    // negalima naudoti min, max;

    MinMAX task3() {

        Integer [] array = {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15};

        MinMAX minMax = new MinMAX();

        for (Integer i: array ){

           if (minMax.max == null){
               minMax.max = i;
           }

           if (minMax.min == null) {
               minMax.min = i;
           }

            minMax.min = minMax.min < i ? minMax.min : i;
            minMax.max = minMax.max > i ? minMax.max : i;

        }
        System.out.println("Array: ");
        printIntArray(array);

        print(minMax);
        return minMax;
    }

    private void print(MinMAX i){
        System.out.println("Min dydis: " + i.min);
        System.out.println("Max value: " + i.max);
    }



    // duotas sveikuju skaiciu masyvas {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15}
    // isrikiuoti didejimo tvarka
    // negalima naudoti Collections.sort(array[Int]);


    void task4() {

        Integer [] array = {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15};

        System.out.print("Initial array: ");
        printIntArray(array);
        System.out.println(" ");

        Integer [] sortedArray = sortArray(array);

        System.out.print("Sorted array: ");
        printIntArray(sortedArray);
    }

    private Integer[] sortArray(Integer[] array){

        boolean anyChanges = true;

        while (anyChanges) {

            anyChanges = false;

            for (Integer i=0; i < array.length - 1; i++) {

                if (array[i] > array[i+1]){

                    Integer temp = array[i];

                    array[i] = array [i+1];

                    array[i+1] = temp;

                    anyChanges = true;
                }
            }
        }

        return array;
    }

   private static void printIntArray (Integer[]array){

        for(Integer i : array) {System.out.print(i + ", ");}
        System.out.println();

    }

}

