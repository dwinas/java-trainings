import com.baltictalents.lessons.work.Company;
import com.baltictalents.lessons.work.WorkTools;
import com.baltictalents.lessons.work.Worker;
import jdk.nashorn.internal.ir.IfNode;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

public class Tasks3 {


    // uzpildyti sveikuju skaiciu masyva fibonaci seka. 1000 elementu.

    void task1() {

        printBigIntegerlsArray(arrayOfFibonaci(1000));
    }


    private BigInteger[] arrayOfFibonaci(Integer j) {

        BigInteger[] arrayOfFibonaci = new BigInteger[j];

        arrayOfFibonaci[0] = BigInteger.ZERO;
        arrayOfFibonaci[1] = BigInteger.ONE;

        for(Integer i = 2; i < arrayOfFibonaci.length; i++) {

            arrayOfFibonaci[i] = arrayOfFibonaci[i-1].add(arrayOfFibonaci[i-2]);

        }
        return arrayOfFibonaci;
    }


     private static void printBigIntegerlsArray (BigInteger[] array){
        for(BigInteger i : array){
            System.out.println(i.toString());
        }
    }




    // parasyti funkcija kuri nustato ar sveikasis skaicius yra pirminis skaicius.

  void task2(int number) {

        if (isPrimary(number)) {System.out.println("Number: " + number + " is primary");}
        else {System.out.println("Number: " + number + " is not primary");}
   }


    private Boolean isPrimary(int number) {

        Boolean hasDivision = false;
        int denominator = 1;
        Boolean isPrimary = false;

        while (hasDivision == false){

            denominator +=1;
            hasDivision = (number % denominator ==0);
        }

        if (number == denominator) { isPrimary = true; }

        return isPrimary;
    }





    // rasti pirmus 100 pirminiu skaiciu ir uzpildyti jais masyva
    void task3() {

        System.out.println("100 pcs of primary numbers: ");
        printIntArray(arrayOfPrimaryNumbers(100));

    }

    private int[] arrayOfPrimaryNumbers(int j) {

        int[] arrayOfPrimaryNumber = new int[j];

        int i = 0;
        int number = 3;

        while (i < j ) {

            if (isPrimary(number)) {
                arrayOfPrimaryNumber[i] = number;
                i += 1;
            }
            number = number + 2;
        }
        return arrayOfPrimaryNumber;
    }

    private static void printIntArray (int[]array){

        for(int i : array) {System.out.print(i + ", ");}
        System.out.println();

    }





//     aprasyti 3 skirtingas klases kurios tarpusavyje turetu logiska sarysi
    void task4() {

        WorkTools kompiuteri = new WorkTools("Lenovo", 900.52, "2018-09-01");
        WorkTools telefonas = new WorkTools("Huawei", 500.0, "2017-03-01");
        WorkTools automobilis = new WorkTools("Nisan", 35000.55, "2016-09-01");

        WorkTools[] toolsOfDirector ={kompiuteri, telefonas, automobilis};

        Worker gediminas = new Worker("Gediminas", "Gediminaitis", "1987-04-09", toolsOfDirector, 13);

        Company fabricOfMilk= new Company("Juru piens", 34, gediminas, "Zariju 6 Vilnius Lithuania");

        fabricOfMilk.printDetails();
        gediminas.printInformation();


    }
}
