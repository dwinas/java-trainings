import java.util.Optional;
import java.util.Scanner;
import java.io.*;

public class Uzduotys1 {

    // programa papraso ivesti skaiciu, i ekrana reikia ivesti ar skaicius lyginis ar nelyginis
    public void task1() {

        System.out.println("test");

        String answer;

        answer = isNumberFlat(readInteger()) ? "Number is flat" : "Number is odd";

        System.out.println(answer);

    }


    private Integer readInteger() {

        Scanner reader = new Scanner(System.in);
        System.out.println("Enter integer number: ");
        Integer number = reader.nextInt();
        reader.close();
        return number;
    }

    private boolean isNumberFlat(Integer number){

        return number % 2 == 0;
    }



    // programa papraso ivesti skaiciu, i ekrana isvedama skaiciaus saknis
    public void task2() {


        String answer = "";
        double number = readDouble();
        answer = number < 0 ? "Number is negative squert is not possible" : answer + Math.sqrt(number);

        System.out.println(answer);

    }

    private double readDouble() {

        double value;
        Scanner scan = new Scanner( System.in );

        System.out.print("Enter a double:");
        value = scan.nextDouble();

        return value;
    }

    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedama kvadratines lygties sprendimas x1 = ?, x2 = ?

    public String task3(Double a, Double b, Double c) {


        Double discriminant = b * b - 4 * a * c;
        String finalAnswer = "";

        if (discriminant < 0) {
            finalAnswer = "No Values";

        } else if (discriminant == 0) {
            finalAnswer = finalAnswer + (-b / 2 * a);
            finalAnswer = "X value = " + ((b + (Math.sqrt(discriminant))) / (2 * a));

        } else {

            finalAnswer = "X1 value is: " + ((b + (Math.sqrt(discriminant))) / (2 * a)) + " X2 value is " + ((b - (Math.sqrt(discriminant))) / (2 * a));
        }

        return finalAnswer;

    }

    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedam kvadratines lygties reiksmes intervale [-5, 5]

    public void task4(Double a, Double b, Double c) {

        Double[] answers = new Double[11];

        Integer l = 0;

        for(Integer i = -5; i <= 5; i++){

            System.out.println("With x value: " + i + " result is: " + (a * i *i + b * i + c));

        }

    }


}
