import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.List;
import  java.nio.file.Path;
import  java.nio.file.Paths;
import  java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.stream.Stream;


// http://www.egzaminai.lt/failai/7417_IT-VBE-1_2018-GALUTINE.pdf pirma uzduotis
// kaip ir minejau paskaitos metu, pabandykime isspresti valstybinio egzamino uzduoti

public class Tasks4 {



    public void runSolution(){

        List <String> filesInformation = readFile();
        System.out.println("File readed");
        Map<String, Integer> informationFromFile = listToMap(filesInformation);

        writeResult(informationFromFile);

    }





    private List <String> readFile(){

        File file = new File("C:\\java\\java\\java-trainings\\src\\com\\baltictalents\\resources\\U1.txt");

        List<String> dataFromFile = new ArrayList<>();

        try {
            Scanner scanedFile = new Scanner(file);
            int i = scanedFile.nextInt();

            while (scanedFile.hasNextLine()) {

                String line = scanedFile.nextLine();
                if (line != " "){
                    dataFromFile.add(line);}
            }
            scanedFile.close();
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
        return dataFromFile;
    }

     private Map<String, Integer> listToMap(List<String> list) {

        Map<String, Integer> map = new HashMap<>();

        for(String i:list ) {

            if (i.length()>1) {

                String spalva = i.substring(0, 1);

                i = i.replaceAll("\\D+", "");

                Integer number = Integer.valueOf(i);

                Integer countedAllready = map.get(spalva);

                if (countedAllready != null) {
                    map.put(spalva, countedAllready + number);
                } else {
                    map.put(spalva, number);
                }
            }
        }
        return map;
    }

     private void writeResult(Map<String, Integer> map) {

        Integer zaliuJuosteliuSkaicius = map.get("Z");
        Integer raudonuJuosteliiuSkaicius = map.get("R");
        Integer geltonuJuosteliuSkaicius = map.get("G");
        Integer veleveliuSkaicius = iseisVeleveliu(zaliuJuosteliuSkaicius, raudonuJuosteliiuSkaicius, geltonuJuosteliuSkaicius);

        String likoZaliu = Integer.toString(zaliuJuosteliuSkaicius - veleveliuSkaicius * 2);
        String likoRaudonu = Integer.toString(raudonuJuosteliiuSkaicius - veleveliuSkaicius * 2);
        String likoGeltonu = Integer.toString(geltonuJuosteliuSkaicius - veleveliuSkaicius * 2);
        try {
            PrintWriter atsakymas = new PrintWriter("C:\\java\\java\\java-trainings\\src\\com\\baltictalents\\resources\\U2Rez.txt", "UTF-8");
            atsakymas.println(Integer.toString(veleveliuSkaicius));
            atsakymas.println("G " + likoGeltonu);
            atsakymas.println("Z " + likoZaliu);
            atsakymas.println("R " + likoRaudonu);
            atsakymas.close();

        }
        catch (IOException ex) {
            // Report
        }

    }



     private Integer iseisVeleveliu(Integer r, Integer g, Integer z){

        Integer maziausiasSkaicius = r;
        maziausiasSkaicius = g < r ? g : r;
        maziausiasSkaicius = z < r ? z : r;

        Integer iseisVeleveliu = maziausiasSkaicius % 2 == 0 ? maziausiasSkaicius / 2 : (maziausiasSkaicius - 1) / 2;

        return iseisVeleveliu;
    }


    }




